//
//  CharacterQuotes.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 09/09/22.
//

import SwiftUI

struct CharacterQuotesBottom: View {
    @Binding var characterQuotes:[CharacterQuote]
    var body: some View {
        if $characterQuotes.count > 0{
            Spacer(minLength: 10)
            Text("Quotes").font(.headline)
            Spacer(minLength: 20)
            let quotes = characterQuotes.map { item in
                return item.quote
            }
            if quotes.count > 0{
                Text(quotes.joined(separator: "\n")).font(.subheadline)
            }
        }
    }
}

//struct CharacterQuotes_Previews: PreviewProvider {
//    static var previews: some View {
//        CharacterQuotes()
//    }
//}
