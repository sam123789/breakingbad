//
//  CharacterDetailBottom.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 09/09/22.
//

import SwiftUI

struct CharacterDetailBottom: View {
    @Binding var character: CharacterDetails
    
    var body: some View {
        VStack(alignment: .leading) {
            
            HStack{
                if character.birthday != ""{
                    Text("Born: "+character.birthday)
                        .font(.subheadline)
                    Spacer()
                }
                if character.status != ""{
                    Text("Status: "+character.status)
                        .font(.subheadline)
                }
            }
            Spacer(minLength: 20)
            HStack{
                if character.nickname != ""{
                    Text("Nickname: "+character.nickname)
                        .font(.subheadline)
                }
            }
            Spacer(minLength: 20)
            HStack{
                if character.appearance.count > 0{
                    Text("BB Appearance: "+character.appearance.map{String($0)}.joined(separator: ","))
                        .font(.subheadline)
                    Spacer()
                }
                if character.better_call_saul_appearance.count>0{
                    Text("BCS Appearance: "+character.better_call_saul_appearance.map{String($0)}.joined(separator: ","))
                        .font(.subheadline)
                }
            }
            Group{
                Spacer(minLength: 20)
                Text("Portrayed: "+character.portrayed)
                    .font(.subheadline)
                Spacer(minLength: 20)
                let occupation = character.occupation.joined(separator: ",")
                Text("Occupation: "+occupation)
                    .font(.subheadline)
                Spacer(minLength: 20)
                Text("Category: "+character.category)
                    .font(.subheadline)
                
                
                
            }
        }
        
    }
}

//struct CharacterDetailBottom_Previews: PreviewProvider {
//    static var previews: some View {
//        CharacterDetailBottom()
//    }
//}
