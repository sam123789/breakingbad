//
//  CharacterDetail.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import SwiftUI

struct CharacterDetail: View {
    
    @State var character: CharacterDetails
    var allCharacters:[CharacterDetails]?
    @State var characterQuotes = [CharacterQuote]()
    @Binding var characterIDS:[Int]
    
    var body: some View {
        ScrollView {
            ZStack(){
                CharacterDetailTop(character: $character)
                HStack{
                    Button {
                        characterQuotes.removeAll()
                        self.getPreviousChracter()
                        self.loadData()
                    } label: {
                        Image(systemName: "chevron.left")
                            .padding()
                            .foregroundColor(.white)
                    }.frame(width: 40, height: 40).offset(CGSize(width: 20, height: 0))
                    Spacer()
                    Button {
                        characterQuotes.removeAll()
                        self.getNextChracter()
                        self.loadData()
                    } label: {
                        Image(systemName: "chevron.right")
                            .padding()
                            .foregroundColor(.white)
                    }.frame(width: 40, height: 40).offset(CGSize(width: -20, height: 0))
                }.frame(height:60)
                VStack{
                    Spacer()
                    HStack{
                        Spacer()
                        Button {
                            if characterIDS.contains(character.char_id){
                                let ind = characterIDS.firstIndex(of: character.char_id)
                                if ind != nil{
                                    characterIDS.remove(at: ind!)
                                }
                            }else{
                                characterIDS.append(character.char_id)
                            }
                        } label: {
                            Image(systemName: characterIDS.contains(character.char_id) == true ? "heart.fill":"heart")
                                .padding()
                                .foregroundColor(.red)
                        }.frame(width: 40, height: 40).offset(CGSize(width: -20, height: 0))
                    }
                }
                
            }.frame(height:200)
            VStack{
                CharacterDetailBottom(character: $character)
                Divider()
                CharacterQuotesBottom(characterQuotes: $characterQuotes)
            }.padding()
        }
        .navigationTitle(character.name)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear(perform: loadData)
        //Added swipe gesture to change characters from left or right but currently conflicting with ScrollView.
        //        .gesture(DragGesture(minimumDistance: 3.0, coordinateSpace: .local)
        //            .onEnded { value in
        //                print(value.translation)
        //                switch(value.translation.width, value.translation.height) {
        //                    case (...0, -30...30):  self.getNextChracter()//print("left swipe")
        //                    case (0..., -30...30):  self.getPreviousChracter()//print("right swipe")
        ////                    case (-100...100, ...0):  print("up swipe")
        ////                    case (-100...100, 0...):  print("down swipe")
        //              let   default:  print("no clue")
        //                }
        //            }
        //        )
    }
    
    func loadData(){
        let path = self.getPath(characterName: character.name)
        ListViewModel(networkManager: HTTPNetworking()).getCharacterQuotes(fromPath: path,completion: { charDetails in
            if charDetails != nil{
                characterQuotes = charDetails!
            }
        })
    }
    
    func getPath(characterName:String)->String{
        var authorName = ""
        if characterName != ""{
            authorName += "?author=\(characterName.replacingOccurrences(of: " ", with: "+"))"
        }
        let path = "quote\(authorName)"
        return path
    }
    
    func getNextChracter(){
        if allCharacters != nil{
            if allCharacters!.count > 0{
                var ind = allCharacters?.firstIndex(of: character)
                if ind != nil{
                    if ind! >= (allCharacters!.count - 1){
                        ind = 0
                        character = allCharacters![ind!]
                    }else{
                        character = allCharacters![ind! + 1]
                    }
                }
            }
        }
    }
    
    func getPreviousChracter(){
        if allCharacters != nil{
            if allCharacters!.count > 0{
                var ind = allCharacters?.firstIndex(of: character)
                if ind != nil{
                    if ind! <= 0{
                        ind = allCharacters!.count - 1
                        character = allCharacters![ind!]
                    }else{
                        character = allCharacters![ind! - 1]
                    }
                }
            }
        }
    }
}

struct CharacterDetail_Previews: PreviewProvider {
    @State static var characterIDS = [1]
    static var previews: some View {
        CharacterDetail(character: characterJsonData[0], characterIDS: $characterIDS)
    }
}
