//
//  CharacterDetailTop.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 09/09/22.
//

import SwiftUI

struct CharacterDetailTop: View {
    @Binding var character: CharacterDetails
    var body: some View {
        ZStack{
        if character.img.contains("http"){
            CacheAsyncImage(url: URL(string: character.img)!, content: { phase in
                switch phase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image.resizable().aspectRatio(1.8, contentMode: .fill).blur(radius: 20)
                    
                case .failure:
                    Image(systemName: "photo")
                @unknown default:
                    // Since the AsyncImagePhase enum isn't frozen,
                    // we need to add this currently unused fallback
                    // to handle any new cases that might be added
                    // in the future:
                    EmptyView()
                }
            })
            CacheAsyncImage(url: URL(string: character.img)!, content: { phase in
                switch phase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image.resizable()
                        .aspectRatio(contentMode: .fit)
                case .failure:
                    Image(systemName: "photo")
                @unknown default:
                    // Since the AsyncImagePhase enum isn't frozen,
                    // we need to add this currently unused fallback
                    // to handle any new cases that might be added
                    // in the future:
                    EmptyView()
                }
            })
        }else{
            //For Model Data preview
            Image(character.img).resizable().aspectRatio(1.8, contentMode: .fill).blur(radius: 20)
            Image(character.img).resizable().aspectRatio(contentMode: .fit)
        }
        }
    }
}

//struct CharacterDetailTop_Previews: PreviewProvider {
//    @Binding static var character = characterJsonData[0]
//    static var previews: some View {
//        CharacterDetailTop(character: $character)
//    }
//}
