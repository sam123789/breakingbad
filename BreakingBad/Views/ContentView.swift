//
//  ContentView.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        CharacterList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
