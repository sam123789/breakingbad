//
//  CharacterList.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import SwiftUI

struct CharacterList: View {
    @State var characters = [CharacterDetails]()
    @State private var searchText = ""
    @State var filterSelected = false
    @State var characterIDS = [Int]()
    
    var body: some View {
        
        NavigationView {
            
            List(filterSelected == true ? filterCharacters : characters) { character in
                NavigationLink {
                    CharacterDetail(character: character,allCharacters: filterSelected == true ? filterCharacters : characters,characterIDS: $characterIDS)
                } label: {
                    CharacterRow(characterDetails: character)
                }.swipeActions(edge: .trailing) {
                    Button(role: .destructive, action: {
                        let characterIndex = filterSelected == true ? filterCharacters.firstIndex(of: character) : characters.firstIndex(of: character)
                        if characterIndex != nil{
                            characters.remove(at: characterIndex!)
                        }
                    } ) {
                        Label("Delete", systemImage: "trash")
                    }
                }
            }.searchable(text: $searchText) {
                ForEach(searchResults) { result in
                    NavigationLink {
                        CharacterDetail(character: result,allCharacters: filterSelected == true ? filterCharacters : characters,characterIDS: $characterIDS)
                    } label: {
                        CharacterRow(characterDetails: result)
                    }.buttonStyle(.plain)
                    
                }
            }.refreshable{
                searchText = ""
                loadData()
            }
            .navigationBarTitle("Breaking Bad", displayMode: .inline)
            .navigationBarItems(trailing:
                                    Button(action: {
                filterSelected.toggle()
            }, label: {
                Image(systemName: self.filterSelected == true ? "heart.fill" : "heart")
                    .padding()
                    .foregroundColor(.red)
            }))
        }.onAppear(perform: loadData)
    }
    
    func loadData(){
        ListViewModel(networkManager:HTTPNetworking()).getCharacters(fromPath:"characters",completion: { charDetails in
            if charDetails != nil{
                characters = charDetails!
            }
        })
    }
    var searchResults: [CharacterDetails] {
        if searchText.isEmpty {
            if filterSelected{
                return filterCharacters
            }else{
                return characters
            }
        } else {
            if filterSelected{
                return filterCharacters.filter { $0.name.contains(searchText) }
            }else{
                return characters.filter { $0.name.contains(searchText) }
            }
            
        }
    }
    var filterCharacters: [CharacterDetails] {
        if filterSelected {
            var items = [CharacterDetails]()
            for characterID in characterIDS{
                let item = characters.first { itemDetail in
                    if itemDetail.char_id == characterID{
                        return true
                    }else{
                        return false
                    }
                }
                if item != nil{
                    items.append(item!)
                }
            }
            return items
        } else {
            return characters
        }
    }
}

struct CharacterList_Previews: PreviewProvider {
    static var previews: some View {
//        ForEach(["iPhone SE (2nd generation)", "iPhone XS Max"], id: \.self) { deviceName in
            CharacterList(characters:characterJsonData)
//                .previewDevice(PreviewDevice(rawValue: deviceName))
//                .previewDisplayName(deviceName)
//        }
    }
}
