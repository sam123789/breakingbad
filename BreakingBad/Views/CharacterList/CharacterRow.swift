//
//  CharacterRow.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import SwiftUI

struct CharacterRow: View {
    @State var characterDetails: CharacterDetails
    
    var body: some View {
        HStack {
            if characterDetails.img.contains("http"){
                CacheAsyncImage(url: URL(string: characterDetails.img)!, content: { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                    case .success(let image):
                        image.resizable()
                            .aspectRatio(contentMode: .fit)
                    case .failure:
                        Image(systemName: "photo")
                    @unknown default:
                        // Since the AsyncImagePhase enum isn't frozen,
                        // we need to add this currently unused fallback
                        // to handle any new cases that might be added
                        // in the future:
                        EmptyView()
                    }
                }).frame(width: 50, height: 50)
            }else{
                characterDetails.image.resizable().aspectRatio(contentMode: .fit).frame(width: 50, height: 50)
            }
            Text(characterDetails.name)
            
            Spacer()
        }
    }
}

struct LandmarkRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CharacterRow(characterDetails: characterJsonData[0])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
