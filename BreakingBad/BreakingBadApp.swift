//
//  BreakingBadApp.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import SwiftUI

@main
struct BreakingBadApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
