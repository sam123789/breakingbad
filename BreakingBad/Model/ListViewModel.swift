//
//  ListViewModel.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 09/09/22.
//

import Foundation

class ListViewModel{
    let networkManager:Networking
    init(networkManager:Networking){
        self.networkManager = networkManager
    }
    func getCharacters(fromPath:String,completion:@escaping([CharacterDetails]?)->Void){
        networkManager.request(fromPath: fromPath) { data, error in
            if error != nil{
                completion(nil)
            }
            let decoded = self.decodeJSON(type:[CharacterDetails].self,from:data)
            completion(decoded)
        }
    }
    
    func getCharacterQuotes(fromPath:String,completion:@escaping([CharacterQuote]?)->Void){
        
        networkManager.request(fromPath: fromPath) { data, error in
            if error != nil{
                completion(nil)
            }
            let decoded = self.decodeJSON(type:[CharacterQuote].self,from:data)
            completion(decoded)
        }
    }
    
    private func decodeJSON<T:Decodable>(type:T.Type,from:Data?)->T?{
        let decoder = JSONDecoder()
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else{
            return nil
        }
        return response
    }
}
