//
//  NetworkManager.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 09/09/22.
//

import Foundation

protocol Networking{
  typealias CompletionHandler = (Data?,Swift.Error?) -> Void
  func request(fromPath:String,completion:@escaping CompletionHandler)
}

struct HTTPNetworking:Networking{
    let baseURL = "https://www.breakingbadapi.com/api/"
  func request(fromPath: String, completion: @escaping CompletionHandler) {
    guard let url = URL(string: baseURL + fromPath) else {return}
    let request = createRequest(from:url)
    let task = createDataTask(from:request,completion:completion)
    task.resume()
  }
  
  private func createRequest(from url:URL)->URLRequest{
    var request = URLRequest(url: url)
    request.cachePolicy = .reloadIgnoringCacheData
    return request
  }
  
  private func createDataTask(from request:URLRequest,completion:@escaping CompletionHandler)->URLSessionDataTask{
    return URLSession.shared.dataTask(with: request) { data, httpResponse, error in
      completion(data,error)
    }
  }
}
