//
//  LocalJSONManager.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 11/09/22.
//

import Foundation



struct LocalJSONManager:Networking{
  func request(fromPath: String, completion: @escaping CompletionHandler) {
      let data: Data

      guard let file = Bundle.main.url(forResource: fromPath, withExtension: nil)
          else {
              completion(nil,fatalError("Couldn't find \(fromPath) in main bundle."))
      }

      do {
          data = try Data(contentsOf: file)
      } catch {
          completion(nil,fatalError("Couldn't load \(fromPath) from main bundle:\n\(error)"))
      }
      completion(data,nil)
  }
}
