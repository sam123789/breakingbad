//
//  CharacterDetails.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import Foundation
import SwiftUI
struct CharacterDetails: Hashable, Codable {
    let char_id: Int
    let name: String
    let birthday: String
    let occupation: [String]
    let img: String
    let status: String
    let nickname: String
    let appearance:[Int]
    let portrayed: String
    let category:String
    let better_call_saul_appearance:[Int]
    var image: Image {
        Image(img)
    }
}

extension CharacterDetails: Identifiable {
    var id: Int { char_id }
}



struct CharacterQuote:Hashable,Codable{
    let quote_id:Int
    let quote:String
    let author:String
}
extension CharacterQuote: Identifiable {
    var id: Int { quote_id }
}
