//
//  BreakingBadTests.swift
//  BreakingBadTests
//
//  Created by smarthaggarwal on 08/09/22.
//

import XCTest
@testable import BreakingBad
import Swinject
import SwinjectAutoregistration

extension CharacterDetails {
    init(data: [String:Any]) {
//    self.init(base: .BTC, amount: amount, currency: .USD)
        self.init(char_id: data["char_id"] as! Int, name: data["name"] as! String, birthday: data["birthday"] as! String, occupation: data["occupation"] as! [String], img: data["img"] as! String, status: data["status"] as! String, nickname: data["nickname"] as! String, appearance: data["appearance"] as! [Int], portrayed: data["portrayed"] as! String, category: data["category"] as! String, better_call_saul_appearance: data["better_call_saul_appearance"] as! [Int])
  }
}
class BreakingBadTests: XCTestCase {
    private let container = Container()
    
    override func setUp() {
      super.setUp()
      container.register(CharacterDetails.self){resolver in
          return CharacterDetails(char_id: 1, name: "Test Character", birthday: "20-03-1978", occupation: ["Teacher","Driver"], img: "https://vignette.wikia.nocookie.net/breakingbad/images/9/95/JesseS5.jpg/revision/latest?cb=20120620012441", status: "Unknown", nickname: "Tester", appearance: [1,2,3], portrayed: "Test Actor", category: "Better Call Saul", better_call_saul_appearance: [1,2,3])
      }
        container.autoregister(CharacterDetails.self,argument: [String:Any].self,initializer:CharacterDetails.init(data:))
    }
    
    override func tearDown() {
      super.tearDown()
      container.removeAll()
    }
    
    // MARK: - Tests
    
    func testCharacterResponseData() {
      let response = container.resolve(CharacterDetails.self)!
        XCTAssertEqual(response.birthday,"20-03-1978")
    }
    
    func testCharacterAutoregisterResponseData(){
        let response = container ~> (CharacterDetails.self, argument:["char_id":2,"name":"Test Character2","birthday":"12-07-1987","occupation":["Electrician","Cleaner"],"img":"","status":"Alive","nickname":"Test2 Nickname","appearance":[1,3,4],"portrayed":"Test Actor2","category":"Breaking Bad","better_call_saul_appearance":[1,3,5]])
//        XCTAssertEqual(response.birthday, "789564")
        XCTAssertEqual(response.birthday, "12-07-1987")
    }
}
